package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.navdrawertraining.data.modelodatos.Categoria;

public class CategoriaUnitTest {

    @Test
    public void emptyConstructorTest() throws NoSuchFieldException, IllegalAccessException {
        final Categoria instance = new Categoria();

        long id_categoria = 0L;
        String nombre_categoria = null;
        String icono_categoria = null;
        String color_categoria = null;

        final Field fieldId = instance.getClass().getDeclaredField("id_categoria");
        fieldId.setAccessible(true);

        final Field fieldNombre = instance.getClass().getDeclaredField("nombre_categoria");
        fieldNombre.setAccessible(true);

        final Field fieldIcono = instance.getClass().getDeclaredField("icono_categoria");
        fieldIcono.setAccessible(true);

        final Field fieldColor = instance.getClass().getDeclaredField("color_categoria");
        fieldColor.setAccessible(true);

        assertEquals("Fields didn't match", fieldId.get(instance), id_categoria);
        assertEquals("Fields didn't match", fieldNombre.get(instance), nombre_categoria);
        assertEquals("Fields didn't match", fieldIcono.get(instance), icono_categoria);
        assertEquals("Fields didn't match", fieldColor.get(instance), color_categoria);
    }

    @Test
    public void constructorTest() throws NoSuchFieldException, IllegalAccessException {
        long id_categoria = 100L;
        String nombre_categoria = "nombre";
        String icono_categoria = "icono";
        String color_categoria = "color";

        final Categoria instance = new Categoria(id_categoria, nombre_categoria, icono_categoria, color_categoria);

        final Field fieldId = instance.getClass().getDeclaredField("id_categoria");
        fieldId.setAccessible(true);

        final Field fieldNombre = instance.getClass().getDeclaredField("nombre_categoria");
        fieldNombre.setAccessible(true);

        final Field fieldIcono = instance.getClass().getDeclaredField("icono_categoria");
        fieldIcono.setAccessible(true);

        final Field fieldColor = instance.getClass().getDeclaredField("color_categoria");
        fieldColor.setAccessible(true);

        assertEquals("Fields didn't match", fieldId.get(instance), id_categoria);
        assertEquals("Fields didn't match", fieldNombre.get(instance), nombre_categoria);
        assertEquals("Fields didn't match", fieldIcono.get(instance), icono_categoria);
        assertEquals("Fields didn't match", fieldColor.get(instance), color_categoria);
    }

    @Test
    public void setId_categoriaTest() throws NoSuchFieldException, IllegalAccessException {
        long idCategoria = 123;
        Categoria instance = new Categoria();
        instance.setId_categoria(idCategoria);
        final Field field = instance.getClass().getDeclaredField("id_categoria");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), idCategoria);
    }
    @Test
    public void getId_categoriaTest() throws NoSuchFieldException, IllegalAccessException {
        final Categoria instance = new Categoria();
        final Field field = instance.getClass().getDeclaredField("id_categoria");
        field.setAccessible(true);
        field.set(instance, 123);

        final long result = instance.getId_categoria();

        assertEquals("Field wasn't retrieved properly", result, 123);
    }

    @Test
    public void setNombre_categoriaTest() throws NoSuchFieldException, IllegalAccessException {
        String nombreCategoria = "Tapas";
        Categoria instance = new Categoria();
        instance.setNombre_categoria(nombreCategoria);
        final Field field =  instance.getClass().getDeclaredField("nombre_categoria");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), nombreCategoria);
    }
    @Test
    public void getNombre_categoriaTest() throws NoSuchFieldException, IllegalAccessException {
        final Categoria instance = new Categoria();
        final Field field = instance.getClass().getDeclaredField("nombre_categoria");
        field.setAccessible(true);
        field.set(instance, "Sushi bueno!");

        final String result = instance.getNombre_categoria();

        assertEquals("Field wasn't retrieved properly", result, "Sushi bueno!");
    }

    @Test
    public void setIcono_categoriaTest() throws NoSuchFieldException, IllegalAccessException {
        String iconoCategoria = "Hello, it's me";
        Categoria instance = new Categoria();
        instance.setIcono_categoria(iconoCategoria);
        final Field field = instance.getClass().getDeclaredField("icono_categoria");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), iconoCategoria);
    }
    @Test
    public void getIcono_categoriaTest() throws NoSuchFieldException, IllegalAccessException {
        final Categoria instance = new Categoria();
        final Field field = instance.getClass().getDeclaredField("icono_categoria");
        field.setAccessible(true);
        field.set(instance, "Hello it's me");

        final String result = instance.getIcono_categoria();

        assertEquals("Field wasn't retrieved properly", result, "Hello it's me");
    }

    @Test
    public void setColor_categoriaTest() throws NoSuchFieldException, IllegalAccessException {
        String colorCategoria = "Hello it's me";
        Categoria instance = new Categoria();
        instance.setColor_categoria(colorCategoria);
        final Field field = instance.getClass().getDeclaredField("color_categoria");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), colorCategoria);
    }
    @Test
    public void getColor_categoriaTest() throws NoSuchFieldException, IllegalAccessException {
        final Categoria instance = new Categoria();
        final Field field = instance.getClass().getDeclaredField("color_categoria");
        field.setAccessible(true);
        field.set(instance, "Hello it's me");

        final String result = instance.getColor_categoria();

        assertEquals("Field wasn't retrieved properly", result, "Hello it's me");
    }
}