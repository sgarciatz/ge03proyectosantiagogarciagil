package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.navdrawertraining.data.modelodatos.Valoracion;

public class ValoracionUnitTest {

    @Test
    public void setId_valoracionTest() throws NoSuchFieldException, IllegalAccessException {
        long value = 123L;
        Valoracion instance = new Valoracion(-1, -1);
        instance.setId_valoracion(value);

        final Field field = instance.getClass().getDeclaredField("id_valoracion");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setPuntuacionTest() throws NoSuchFieldException, IllegalAccessException {
        float value = 4.5F;
        Valoracion instance = new Valoracion(-1, -1);
        instance.setPuntuacion(value);

        final Field field = instance.getClass().getDeclaredField("puntuacion");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getId_valoracion () throws NoSuchFieldException, IllegalAccessException {
        final Valoracion instance = new Valoracion(-1, -1);
        final Field field = instance.getClass().getDeclaredField("id_valoracion");
        field.setAccessible(true);

        field.set(instance, 123L);

        long result = instance.getId_valoracion();

        assertEquals("field wasn't retireved properly", result, 123L);
    }

    @Test
    public void getPuntuacion () throws NoSuchFieldException, IllegalAccessException {
        final Valoracion instance = new Valoracion(-1, -1);
        final Field field = instance.getClass().getDeclaredField("puntuacion");
        field.setAccessible(true);

        field.set(instance, 4.5F);

        float result = instance.getPuntuacion();

        assertEquals("field wasn't retireved properly", result, 4.5F, 0.001F);
    }
}
