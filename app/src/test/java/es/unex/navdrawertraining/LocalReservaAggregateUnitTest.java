package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Date;

import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.modelodatos.Reserva;
import es.unex.navdrawertraining.helpers.LocalReservaAggregate;

public class LocalReservaAggregateUnitTest {
    @Test
    public void getLocalTest() throws NoSuchFieldException, IllegalAccessException{
        final LocalReservaAggregate  instance = new LocalReservaAggregate(new Local(), new Reserva(-1, new Date()));
        final Field field = instance.getClass().getDeclaredField("local");
        field.setAccessible(true);
        field.set(instance, new Local());

        //when
        final Local result = instance.getLocal();
        //then
        assertEquals("Fields didn't match", field.get(instance), result);
    }

    @Test
    public void getReservaTest() throws NoSuchFieldException, IllegalAccessException{
        final LocalReservaAggregate  instance = new LocalReservaAggregate(new Local(), new Reserva(-1, new Date()));
        final Field field = instance.getClass().getDeclaredField("reserva");
        field.setAccessible(true);
        field.set(instance, new Reserva(-2, new Date()));

        //when
        final Reserva result = instance.getReserva();
        //then
        assertEquals("Fields didn't match", field.get(instance), result);
    }

}
