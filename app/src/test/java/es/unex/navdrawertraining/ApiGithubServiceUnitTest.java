package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.repoexterno.GithubService;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiGithubServiceUnitTest {

    @Test
    public void getLocalesTest() throws IOException {
        List<Local> locales = new ArrayList<>();
        Local l1 = new Local();
        l1.setId(1);
        l1.setNombre("Urban's");
        l1.setDescripcion("Buen sitio");
        List<Double> coord = new ArrayList<Double>();
        coord.add(39.48);
        coord.add(-6.37);
        l1.setCoordenadas(coord);
        l1.setValoracionGlobal(4);
        l1.setFotoPerfil("fotoPerfil");
        l1.setFotoBackground("fotoBackground");
        l1.setAforoTotal(100);
        l1.setOcupacionActual(40);
        List<String> tags = new ArrayList<>();
        tags.add("raciones");
        tags.add("desayunos");
        l1.setEtiquetas(tags);

        Local l2 = new Local();
        l2.setId(2);
        l2.setNombre("Meraki");
        l2.setDescripcion("Genial sitio");
        List<Double> coord2 = new ArrayList<Double>();
        coord2.add(39.48);
        coord2.add(-6.37);
        l2.setCoordenadas(coord2);
        l2.setValoracionGlobal(4.5F);
        l2.setFotoPerfil("fotoPerfil2");
        l2.setFotoBackground("fotoBackground2");
        l2.setAforoTotal(150);
        l2.setOcupacionActual(30);
        List<String> tags2 = new ArrayList<>();
        tags2.add("cerveza");
        tags2.add("pinchos");
        l2.setEtiquetas(tags2);

        locales.add(l1);
        locales.add(l2);

        ObjectMapper objectMapper = new ObjectMapper();

        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MockResponse mockResponse = new MockResponse();
        mockResponse.setResponseCode(200);
        mockResponse.setBody(objectMapper.writeValueAsString(locales));
        mockWebServer.enqueue(mockResponse);
        //we link the mock server with our retrofit api
        GithubService service = retrofit.create(GithubService.class);

        Call<List<Local>> call = service.listLocales();

        Response<List<Local>> response = call.execute();

        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        List<Local> localResponse = response.body();
        assertTrue(localResponse.size() == 2);
        final String aux = localResponse.get(0).getNombre();
        assertEquals("Fields do not match", aux, "Urban's");
        final String aux2 = localResponse.get(1).getNombre();
        assertEquals("Fields do not match", aux2, "Meraki");

        mockWebServer.shutdown();
    }
}
