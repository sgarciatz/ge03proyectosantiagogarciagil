package es.unex.navdrawertraining;

import android.content.Context;

import es.unex.navdrawertraining.data.repository.CategoriaRepository;
import es.unex.navdrawertraining.data.repository.ComentarioRepository;
import es.unex.navdrawertraining.data.repository.FavoritoRepository;
import es.unex.navdrawertraining.data.repository.LocalCategoriaRepository;
import es.unex.navdrawertraining.data.repository.LocalRepository;
import es.unex.navdrawertraining.data.repository.ReservaRepository;
import es.unex.navdrawertraining.data.repository.ValoracionRepository;
import es.unex.navdrawertraining.data.repoexterno.LocalNetworkDataSource;
import es.unex.navdrawertraining.data.roomdb.CeresLimitDatabase;
import es.unex.navdrawertraining.ui.busqueda.BusquedaViewModelFactory;
import es.unex.navdrawertraining.ui.categorias.CategoriaViewModelFactory;
import es.unex.navdrawertraining.ui.categorias.LocalesCategoriaViewModelFactory;
import es.unex.navdrawertraining.ui.favoritos.FavoritoViewModelFactory;
import es.unex.navdrawertraining.ui.home.HomeViewModelFactory;
import es.unex.navdrawertraining.ui.detalles.DetallesViewModelFactory;
import es.unex.navdrawertraining.ui.reservas.ReservaViewModelFactory;

public class DIContainer {
    //Fields for the source of the application info
    private final CeresLimitDatabase database;
    private final LocalNetworkDataSource networkDataSource;

    //Fields for the Repository classes of the application
    public final LocalRepository mLocalRepository;
    public final CategoriaRepository mCategoriaRepository;
    public final LocalCategoriaRepository mLocalCategoriaRepository;
    public final FavoritoRepository mFavoritoRepository;
    public final ReservaRepository mReservaRepository;
    public final ComentarioRepository mComentarioRepository;
    public final ValoracionRepository mValoracionRepository;

    //Fields for the ViewModelFactory classes of the application
    public final HomeViewModelFactory mHomeViewModelFactory;
    public final ReservaViewModelFactory mReservaViewModelFactory;
    public final FavoritoViewModelFactory mFavoritoViewModelFactory;
    public final DetallesViewModelFactory mDetallesViewModelFactory;
    public final BusquedaViewModelFactory mBusquedaViewModelFactory;
    public final CategoriaViewModelFactory mCategoriaViewModelFactory;
    public final LocalesCategoriaViewModelFactory mLocalesCategoriaViewModelFactory;

    public DIContainer(Context context) {
        database = CeresLimitDatabase.getInstance(context);
        networkDataSource = LocalNetworkDataSource.getInstance();

        //Instantiation of Repository classes
        mLocalRepository = LocalRepository.getInstance(database.getDaoLocal(), networkDataSource);
        mCategoriaRepository = CategoriaRepository.getsInstance(database.getDaoCategoria());
        mLocalCategoriaRepository = LocalCategoriaRepository.getInstance(database.getDaoLocalCategoria());
        mFavoritoRepository = FavoritoRepository.getsInstance(database.getDaoFavorito());
        mReservaRepository = ReservaRepository.getInstance(database.getDaoReserva());
        mComentarioRepository = ComentarioRepository.getInstance(database.getDaoComentario());
        mValoracionRepository = ValoracionRepository.getInstance(database.getDaoValoracion());

        //Instantiation of ViewModelFactory classes
        mHomeViewModelFactory = new HomeViewModelFactory(mLocalRepository, mCategoriaRepository);
        mReservaViewModelFactory = new ReservaViewModelFactory(mReservaRepository);
        mFavoritoViewModelFactory = new FavoritoViewModelFactory(mFavoritoRepository);
        mDetallesViewModelFactory = new DetallesViewModelFactory(mComentarioRepository, mValoracionRepository, mFavoritoRepository);
        mBusquedaViewModelFactory = new BusquedaViewModelFactory(mLocalRepository);
        mCategoriaViewModelFactory = new CategoriaViewModelFactory(mCategoriaRepository);
        mLocalesCategoriaViewModelFactory = new LocalesCategoriaViewModelFactory(mLocalCategoriaRepository);
    }
}
