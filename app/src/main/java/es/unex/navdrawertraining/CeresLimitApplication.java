package es.unex.navdrawertraining;

import android.app.Application;


public class CeresLimitApplication extends Application {
    public DIContainer container;

    @Override
    public void onCreate() {
        super.onCreate();

        container = new DIContainer(this); //Instantiate here because the context is needed for the Room database
    }
}
