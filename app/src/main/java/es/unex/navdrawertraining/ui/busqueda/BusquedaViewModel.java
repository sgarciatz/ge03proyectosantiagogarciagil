package es.unex.navdrawertraining.ui.busqueda;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.navdrawertraining.data.repository.LocalRepository;
import es.unex.navdrawertraining.data.modelodatos.Local;

public class BusquedaViewModel extends ViewModel {
    private final LiveData<List<Local>> locales;

    public BusquedaViewModel(LocalRepository localRepository) {
        locales = localRepository.getCurrentLocales();
    }

    public LiveData<List<Local>> getLocales() {
        return locales;
    }
}
