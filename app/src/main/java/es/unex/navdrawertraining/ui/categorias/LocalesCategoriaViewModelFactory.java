package es.unex.navdrawertraining.ui.categorias;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.navdrawertraining.data.repository.LocalCategoriaRepository;

public class LocalesCategoriaViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final LocalCategoriaRepository mLocalCategoriaRepository;

    public LocalesCategoriaViewModelFactory(LocalCategoriaRepository mLocalCategoriaRepository) {
        this.mLocalCategoriaRepository = mLocalCategoriaRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new LocalesCategoriaViewModel(mLocalCategoriaRepository);
    }
}
