package es.unex.navdrawertraining.ui.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.unex.navdrawertraining.R;
import es.unex.navdrawertraining.data.modelodatos.Local;

public class HomeLocalesAdapter extends RecyclerView.Adapter<HomeLocalesAdapter.ViewHolder> {
    private List<Local> locales;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nombre;
        private final ImageView background;
        private final ImageView icono;
        private final RatingBar rating;
        private final CardView container;
        public ViewHolder(@NonNull View view) {
            super(view);
            nombre = (TextView) view.findViewById(R.id.tv_home_item);
            background = (ImageView) view.findViewById(R.id.iv_background_local);
            icono = (ImageView) view.findViewById(R.id.iv_icono_local);
            rating = (RatingBar) view.findViewById(R.id.rb_rating_local);
            container = (CardView) view.findViewById(R.id.cardview_item);
        }

        public TextView getNombre() {
            return nombre;
        }

        public ImageView getBackground() { return background; }

        public ImageView getIcono() { return icono; }

        public RatingBar getRating() { return rating; }

        public CardView getContainer() {
            return container;
        }
    }

    public HomeLocalesAdapter() {
        locales = new ArrayList<Local>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_home_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getNombre().setText(locales.get(position).getNombre());
        holder.getRating().setRating(locales.get(position).getValoracionGlobal());
        Picasso.get().load(locales.get(position).getFotoPerfil()).into(holder.icono);
        Picasso.get().load(locales.get(position).getFotoBackground()).into(holder.background);

        holder.getContainer().setOnClickListener(view -> Navigation.findNavController(view).navigate(R.id.action_nav_home_to_nav_detalles, locales.get(holder.getAdapterPosition()).bundlelizeLocal()));
    }

    @Override
    public int getItemCount() {
        return (locales.size() / 3) % 21; // Show only a bunch of them
    }

    public void swap(List<Local> locales) {
        this.locales =  new ArrayList<>(locales);
        for(int i = 0; i < this.locales.size(); i++) {
            if (this.locales.get(i).getOcupacionActual().intValue() == this.locales.get(i).getAforoTotal().intValue()) {
                this.locales.remove(i);
            }
        }
        Collections.sort(this.locales);
        notifyDataSetChanged();
    }
}
