package es.unex.navdrawertraining.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.navdrawertraining.data.repository.CategoriaRepository;
import es.unex.navdrawertraining.data.repository.LocalRepository;
import es.unex.navdrawertraining.data.modelodatos.Categoria;
import es.unex.navdrawertraining.data.modelodatos.Local;

public class HomeViewModel extends ViewModel {
    private final LiveData<List<Local>> locales;
    private final LiveData<List<Categoria>> categorias;

    public HomeViewModel(LocalRepository localRepository, CategoriaRepository categoriaRepository) {
        locales = localRepository.getCurrentLocales();
        categorias = categoriaRepository.getCategorias();
    }

    public LiveData<List<Local>> getLocales() {
        return locales;
    }

    public LiveData<List<Categoria>> getCategorias() {
        return categorias;
    }
}

