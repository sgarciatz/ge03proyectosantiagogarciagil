package es.unex.navdrawertraining.ui.categorias;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.navdrawertraining.data.repository.CategoriaRepository;
import es.unex.navdrawertraining.data.modelodatos.Categoria;

public class CategoriaViewModel extends ViewModel {
    private final LiveData<List<Categoria>> categorias;

    public CategoriaViewModel (CategoriaRepository categoriaRepository) {
        categorias = categoriaRepository.getCategorias();
    }

    public LiveData<List<Categoria>> getCategorias() {
        return categorias;
    }
}
