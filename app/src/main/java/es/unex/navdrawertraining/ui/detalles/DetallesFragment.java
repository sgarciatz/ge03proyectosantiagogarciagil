package es.unex.navdrawertraining.ui.detalles;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.preference.PreferenceManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import es.unex.navdrawertraining.AppExecutors;
import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.R;
import es.unex.navdrawertraining.databinding.FragmentDetallesBinding;
import es.unex.navdrawertraining.data.modelodatos.Favorito;
import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.repoexterno.LocalesNetworkLoaderRunnable;
import es.unex.navdrawertraining.data.roomdb.CeresLimitDatabase;
import es.unex.navdrawertraining.data.roomdb.FavoritoDao;

public class DetallesFragment extends Fragment{
    private FragmentDetallesBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentDetallesBinding.inflate(inflater, container, false);

        //Name of the Local
        TextView nombreLocal = binding.tvNombreLocal;
        nombreLocal.setText(getArguments().getString("nombre"));

        //Description of the Local
        TextView descripcion = binding.tvDescipcion;
        descripcion.setText(getArguments().getString("descripcion"));

        //Background and profile images
        Picasso.get().load(getArguments().getString("fotoPerfil")).into(binding.ivLogoLocal);
        Picasso.get().load(getArguments().getString("fotoBackground")).into(binding.ivBackgroundLocal);

        //Progress bar and the inner text
        ProgressBar progressBar = binding.progressBar;
        int capacidad = getArguments().getInt("ocupacionActual") * 100 / getArguments().getInt("aforoTotal") ;
        progressBar.setProgress(capacidad);
        binding.tvDetallesNumaforo.setText(getArguments().getInt("ocupacionActual") + " / " + getArguments().getInt("aforoTotal"));

        //Aforo updating via swipe
        binding.swipeRlayout.setOnRefreshListener(() -> AppExecutors.getInstance().networkIO().execute(new LocalesNetworkLoaderRunnable(locales -> {
            for(Local l: locales){
                if(l.getId() == getArguments().getLong("id")) {
                    int oc=(int)(Math.random() * (l.getAforoTotal()- l.getOcupacionActual()) + l.getOcupacionActual());
                    int cp= oc * 100 / l.getAforoTotal();
                    //This simulates that the capacity of the local has changed since the last time it was fetch
                    //by using a random function. But it is fetch from the external repository to make it more "realistic"
                    getActivity().runOnUiThread(() ->   {progressBar.setProgress(cp);
                        binding.tvDetallesNumaforo.setText(oc + " / " + getArguments().getInt("aforoTotal"));} );
                    break;
                }
            }
            getActivity().runOnUiThread(() -> binding.swipeRlayout.setRefreshing(false));
        })));

        //Listener for reserve
        binding.fbReservar.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putLong("id",getArguments().getLong("id"));
            bundle.putString("nombre", getArguments().getString("nombre"));
            bundle.putString("fotoPerfil", getArguments().getString("fotoPerfil"));
            bundle.putString("fotoBackground", getArguments().getString("fotoBackground"));
            Navigation.findNavController(view).navigate(R.id.action_nav_detalles_to_nav_hacer_reserva2, bundle);
        });

        //Rating bar
        RatingBar rbValoracion = binding.ratingbarLocal;

        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the DetallesViewModel that outlives the Fragment lifecyle
        DetallesViewModel detallesViewModel = new ViewModelProvider(this, diContainer.mDetallesViewModelFactory).get(DetallesViewModel.class);

        //Set the id of the Local and observe changes in the LiveData fields of DetallesViewModel
        detallesViewModel.setId_local(getArguments().getLong("id"));

        //Observe changes in the rating
        detallesViewModel.getValoracion().observe(this.getViewLifecycleOwner(), valoracion -> {
            if (valoracion != null) {
                binding.sinValoracion.setVisibility(View.GONE);
                binding.ratingbarLocal.setRating(valoracion.getPuntuacion());
            }
        });
        //Observe changes in the comment
        detallesViewModel.getComentario().observe(this.getViewLifecycleOwner(), comentario -> {
            if (comentario != null) {
                binding.tvCuerpoComentario.setText(comentario.getCadena_comentario());
                String nombreUsuario = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("nombreUsuario", "Kiko");
                if (nombreUsuario.isEmpty()) {
                    nombreUsuario = "Kiko";
                }
                binding.tvNombreUsuario.setText(nombreUsuario);
                binding.tvDetalles5mentarios.setVisibility(View.GONE);
                binding.cardviewDetallesComentario.setVisibility(View.VISIBLE);
            }
        });

        //Listener for reviewing
        binding.bResena.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putLong("id", getArguments().getLong("id"));
            bundle.putString("nombre", getArguments().getString("nombre"));
            bundle.putString("fotoPerfil", getArguments().getString("fotoPerfil"));
            bundle.putString("fotoBackground", getArguments().getString("fotoBackground"));
            Navigation.findNavController(view).navigate(R.id.detalles_to_resena_action, bundle);

        });

        //Checking if the local is in the favourite list or not

        detallesViewModel.getFavorito().observe(this.getViewLifecycleOwner(), favorito -> {
            if (favorito != null){
                binding.ibDetallesFavorito.setImageResource(R.drawable.icono_favorito);
            } else {
                binding.ibDetallesFavorito.setImageResource(R.drawable.icono_nofav);
            }
            }
        );

        //Listener for adding/remove the Local to the favourite list
        binding.ibDetallesFavorito.setOnClickListener(view -> AppExecutors.getInstance().diskIO().execute(() -> {
            FavoritoDao favoritoDao = CeresLimitDatabase.getInstance(getContext()).getDaoFavorito();
            if(detallesViewModel.getFavorito().getValue() != null) {
                favoritoDao.delete(getArguments().getLong("id"));
            } else {
                favoritoDao.insert(new Favorito(getArguments().getLong("id")));
            }
        }));

        //Listener for adding a local to a category
        binding.ibDetallesCategoria.setOnClickListener(view -> {
            Bundle bundle = new Bundle ();
            bundle.putLong("id", getArguments().getLong("id"));
            Navigation.findNavController(view).navigate(R.id.anadir_local_categoria_action, bundle);
        });

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}