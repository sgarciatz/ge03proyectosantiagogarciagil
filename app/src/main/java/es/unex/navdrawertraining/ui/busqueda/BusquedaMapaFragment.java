package es.unex.navdrawertraining.ui.busqueda;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.preference.PreferenceManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import es.unex.navdrawertraining.AppExecutors;
import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.R;
import es.unex.navdrawertraining.data.modelodatos.Local;

public class BusquedaMapaFragment extends Fragment{
    private GoogleMap mGoogleMap;

    private OnMapReadyCallback callback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mGoogleMap = googleMap;
            mGoogleMap.setOnInfoWindowClickListener(marker -> Navigation.findNavController(getView()).navigate(R.id.action_nav_mapa_to_nav_detalles, ((Local) marker.getTag()).bundlelizeLocal()));

            LatLng caceres = new LatLng(39.475107, -6.371448);
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map_style));
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            int zoom = Integer.parseInt(preferences.getString("zoom", "15"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(caceres, zoom));

            //Get an instance of the dependency injector container
            DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

            //Get an instance of the BusquedaViewModel that outlives the Fragment lifecyle
            BusquedaViewModel busquedaViewModel = new ViewModelProvider(BusquedaMapaFragment.this, diContainer.mBusquedaViewModelFactory).get(BusquedaViewModel.class);

            //Observe changes over the LiveData field containting Local items in BusquedaViewModel
            busquedaViewModel.getLocales().observe(BusquedaMapaFragment.this.getViewLifecycleOwner(), locales -> {
                for (Local l : locales) {
                    AppExecutors.getInstance().networkIO().execute(() -> {
                        try {
                            MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(l.getCoordenadas().get(0), l.getCoordenadas().get(1))).title(l.getNombre());
                            Bitmap icono = Bitmap.createScaledBitmap(Picasso.get().load(l.getFotoPerfil()).get(), 150, 150, false);
                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icono));
                            AppExecutors.getInstance().mainThread().execute(() -> {
                                Marker marker = mGoogleMap.addMarker(markerOptions);
                                marker.setTag(l);
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
            });

        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_busqueda_mapa, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }

}