package es.unex.navdrawertraining.ui.detalles;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.navdrawertraining.data.modelodatos.Favorito;
import es.unex.navdrawertraining.data.repository.ComentarioRepository;
import es.unex.navdrawertraining.data.repository.FavoritoRepository;
import es.unex.navdrawertraining.data.repository.ValoracionRepository;
import es.unex.navdrawertraining.data.modelodatos.Comentario;
import es.unex.navdrawertraining.data.modelodatos.Valoracion;

public class DetallesViewModel extends ViewModel {
    private final ComentarioRepository mComentarioRepository;
    private final ValoracionRepository mValoracionRepository;
    private final FavoritoRepository mFavoritoRepository;
    private final LiveData<Comentario> comentario;
    private final LiveData<Valoracion> valoracion;
    private final LiveData<Favorito> favorito;

    public DetallesViewModel(ComentarioRepository mComentarioRepository, ValoracionRepository mValoracionRepository, FavoritoRepository mFavoritoRepository) {
        this.mComentarioRepository = mComentarioRepository;
        this.mValoracionRepository = mValoracionRepository;
        this.mFavoritoRepository = mFavoritoRepository;
        comentario = this.mComentarioRepository.getComentario();
        valoracion = this.mValoracionRepository.getValoracion();
        favorito = this.mFavoritoRepository.getFavorito();
    }


    public LiveData<Comentario> getComentario() {
        return comentario;
    }

    public LiveData<Valoracion> getValoracion() {
        return valoracion;
    }

    public LiveData<Favorito> getFavorito() {
        return favorito;
    }

    public void setId_local(long id_local) {
        mComentarioRepository.setLocalId(id_local);
        mValoracionRepository.setLocalId(id_local);
        mFavoritoRepository.setIdLocal(id_local);
    }


}
