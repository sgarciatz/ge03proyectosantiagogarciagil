package es.unex.navdrawertraining.ui.busqueda;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.navdrawertraining.data.repository.LocalRepository;

public class BusquedaViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final LocalRepository mLocalRepository;

    public BusquedaViewModelFactory(LocalRepository mLocalRepository) {
        this.mLocalRepository = mLocalRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new BusquedaViewModel(mLocalRepository);
    }
}
