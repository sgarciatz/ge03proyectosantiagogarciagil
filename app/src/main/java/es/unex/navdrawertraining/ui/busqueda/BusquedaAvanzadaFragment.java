package es.unex.navdrawertraining.ui.busqueda;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.data.repository.LocalRepository;
import es.unex.navdrawertraining.databinding.FragmentBusquedaAvanzadaBinding;

public class BusquedaAvanzadaFragment extends Fragment{
    private FragmentBusquedaAvanzadaBinding binding;

    private LocalRepository mLocalRepository;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        binding = FragmentBusquedaAvanzadaBinding.inflate(inflater, container, false);


        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the BusquedaViewModel that outlives the Fragment lifecyle
        BusquedaViewModel busquedaViewModel = new ViewModelProvider(this, diContainer.mBusquedaViewModelFactory).get(BusquedaViewModel.class);

        //Prepare the RecyclerView containing Locales
        RecyclerView recyclerView = binding.rvBusquedaAvanzada;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        BusquedaAvanzadaAdapter mBusquedaAvanzadaAdapter = new BusquedaAvanzadaAdapter();
        recyclerView.setAdapter(mBusquedaAvanzadaAdapter);

        //Observe changes over the LiveData field, containing Local items, of BusquedaViewModel
        busquedaViewModel.getLocales().observe(this.getViewLifecycleOwner(), locales -> mBusquedaAvanzadaAdapter.swap(locales));

        //Listener to get all Local items despite of their actual capacity
        RadioButton todos = binding.rbBusquedaAvanzadaTodos;
        todos.setOnClickListener(view -> {
            mBusquedaAvanzadaAdapter.setModo(1);
            mBusquedaAvanzadaAdapter.filter();
        });

        //Listener to get all Local items that have remaining space
        RadioButton consitio = binding.rbBusquedaAvanzadaConsitio;
        consitio.setOnClickListener(view -> {
            mBusquedaAvanzadaAdapter.setModo(2);
            mBusquedaAvanzadaAdapter.filter();
        });

        //Definition of the SearchBar behaviour
        SearchView searchView =  binding.svBusquedaAvanzada;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mBusquedaAvanzadaAdapter.setQuery(query);
                mBusquedaAvanzadaAdapter.filter();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mBusquedaAvanzadaAdapter.setQuery(newText);
                mBusquedaAvanzadaAdapter.filter();
                return true;
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}