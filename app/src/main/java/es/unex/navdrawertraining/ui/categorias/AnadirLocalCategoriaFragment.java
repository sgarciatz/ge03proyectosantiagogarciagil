package es.unex.navdrawertraining.ui.categorias;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import es.unex.navdrawertraining.CeresLimitApplication;
import es.unex.navdrawertraining.DIContainer;
import es.unex.navdrawertraining.databinding.FragmentAnadirLocalCategoriaBinding;

public class AnadirLocalCategoriaFragment extends Fragment{
    private FragmentAnadirLocalCategoriaBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentAnadirLocalCategoriaBinding.inflate(inflater, container, false);

        //Get an instance of the dependency injector container
        DIContainer diContainer = ((CeresLimitApplication) getActivity().getApplication()).container;

        //Get an instance of the CategoriaViewModel that outlives the Fragment lifecyle
        CategoriaViewModel categoriaViewModel = new ViewModelProvider(this, diContainer.mCategoriaViewModelFactory).get(CategoriaViewModel.class);

        //Prepare the RecyclerView containing Categorias
        RecyclerView recyclerView = binding.rvAnadirlocalcategoria;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        AnadirLocalCategoriaAdapter mAdapter= new AnadirLocalCategoriaAdapter(getArguments().getLong("id"), getActivity());
        recyclerView.setAdapter(mAdapter);

        //Observe changes over the LiveData field, containing Categoria items, of CategoriaViewModel
        categoriaViewModel.getCategorias().observe(this.getViewLifecycleOwner(), categorias -> mAdapter.swap(categorias));

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}