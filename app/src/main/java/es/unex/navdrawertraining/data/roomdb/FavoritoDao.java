package es.unex.navdrawertraining.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Favorito;
import es.unex.navdrawertraining.data.modelodatos.Local;

@Dao
public interface FavoritoDao {
    @Query("SELECT l.* FROM Favorito f JOIN Local l ON f.id_favorito = l.id")
    public LiveData<List<Local>> getAll();

    @Query("SELECT * FROM Favorito WHERE id_favorito= :id")
    public LiveData<Favorito> get(long id);

    @Insert
    public long insert(Favorito favorito);

    @Query("DELETE FROM Favorito WHERE id_favorito = :id")
    public void delete(long id);
}
