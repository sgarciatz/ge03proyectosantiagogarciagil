package es.unex.navdrawertraining.data.modelodatos;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName="Favorito")
public class Favorito {

    @Ignore
    public final static String ID_FAVORITO = "id_favorito";

    @PrimaryKey(autoGenerate = false)
    private long id_favorito;

    public Favorito(long id_favorito) {
        this.id_favorito = id_favorito;
    }

    public long getId_favorito() {
        return id_favorito;
    }

    public void setId_favorito(long id_favorito) {
        this.id_favorito = id_favorito;
    }
}
