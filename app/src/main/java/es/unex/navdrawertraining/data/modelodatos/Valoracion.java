package es.unex.navdrawertraining.data.modelodatos;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity(tableName="Valoracion")
public class Valoracion {
    @Ignore
    public final static String ID_VALORACION = "id_valoracion";

    @Ignore
    public final static String PUNTUACION = "puntuacion";

    @PrimaryKey(autoGenerate = false)
    private long id_valoracion;

    @ColumnInfo(name = "puntuacion")
    private float puntuacion;

    public Valoracion(long id_valoracion, float puntuacion) {
        this.id_valoracion=id_valoracion;
        this.puntuacion=puntuacion;
    }

    public long getId_valoracion() {
        return id_valoracion;
    }

    public void setId_valoracion(long id_valoracion) {
        this.id_valoracion = id_valoracion;
    }

    public float getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(float puntuacion) {
        this.puntuacion = puntuacion;
    }
}
