package es.unex.navdrawertraining.data.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Favorito;
import es.unex.navdrawertraining.data.modelodatos.Local;
import es.unex.navdrawertraining.data.roomdb.FavoritoDao;

public class FavoritoRepository {
    private static final String LOG_TAG = FavoritoRepository.class.getSimpleName();

    private final FavoritoDao mFavoritoDao;
    private static FavoritoRepository sInstance;
    private final LiveData<List<Local>> mLocalesLiveList;
    private final MutableLiveData<Long> idLocal = new MutableLiveData<>();

    private FavoritoRepository (FavoritoDao favoritoDao) {
        Log.i(LOG_TAG, "Fetching the favourites locales from the database");
        mFavoritoDao = favoritoDao;
        mLocalesLiveList = mFavoritoDao.getAll();
    }
    public synchronized static FavoritoRepository getsInstance (FavoritoDao favoritoDao) {
        Log.i(LOG_TAG, "Getting the FavoritoRepository");
        if (sInstance == null) {
            sInstance = new FavoritoRepository(favoritoDao);
            Log.i(LOG_TAG, "Made the instance of the Favoritos Repository");
        }
        return sInstance;
    }

    public LiveData<List<Local>> getLocalesFavoritos() {
        return mLocalesLiveList;
    }

    public void setIdLocal (long idLocal) {
        this.idLocal.setValue(idLocal);
    }

    public LiveData<Favorito> getFavorito () {
        return Transformations.switchMap(idLocal, newId -> mFavoritoDao.get(newId));
    }
}
