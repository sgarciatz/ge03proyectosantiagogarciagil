package es.unex.navdrawertraining.data.roomdb;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EtiquetasConverter {
    @TypeConverter
    public static String etiquetasToString (List<String> etiquetas) {
        String etiquetasConcat = "";
        for (String tag: etiquetas) {
            etiquetasConcat = etiquetasConcat + "," + tag;
        }
        etiquetasConcat.substring(1);
        return etiquetasConcat;
    }

    @TypeConverter
    public static List<String> stringToEtiquetas (String etiquetas) {
        if (etiquetas != null) {
            String[] stringEtiquetas = etiquetas.split(",");
            List<String> tagList = new ArrayList<>();
            Collections.addAll(tagList, stringEtiquetas);
            return tagList;
        } else {
            return null;
        }
    }
}
