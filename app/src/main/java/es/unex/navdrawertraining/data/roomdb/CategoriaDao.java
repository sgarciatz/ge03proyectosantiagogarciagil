package es.unex.navdrawertraining.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.navdrawertraining.data.modelodatos.Categoria;

@Dao
public interface CategoriaDao {
    @Query("SELECT * FROM Categoria")
    public LiveData<List<Categoria>> getAll();

    @Insert
    public long insert(Categoria categoria);

    @Query("DELETE FROM Categoria WHERE id_categoria = :id")
    public void delete(long id);
}
