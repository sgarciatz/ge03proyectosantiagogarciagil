package es.unex.navdrawertraining.data.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.navdrawertraining.data.roomdb.ReservaDao;
import es.unex.navdrawertraining.helpers.LocalReservaAggregate;

public class ReservaRepository {
    private static final String LOG_TAG = ReservaRepository.class.getSimpleName();

    private static ReservaRepository sInstance;
    private final LiveData<List<LocalReservaAggregate>> mReservasLiveList;

    private ReservaRepository(ReservaDao mReservaDao) {
        mReservasLiveList = mReservaDao.getAll();
    }

    public synchronized static ReservaRepository getInstance(ReservaDao reservaDao) {
        Log.i(LOG_TAG, "Getting the ReservaViewModel");
        if (sInstance == null) {
            sInstance = new ReservaRepository(reservaDao);
            Log.i(LOG_TAG, "Made the instance of the Reserva Respository");
        }
        return sInstance;
    }

    public LiveData<List<LocalReservaAggregate>> getReservas() { return mReservasLiveList; }
}
