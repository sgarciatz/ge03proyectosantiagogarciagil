package es.unex.navdrawertraining.data.roomdb;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.List;

public class CoordenadasConverter {
    @TypeConverter
    public static String coordenadasToString (List<Double> coordenadas) {
        return coordenadas == null ? null : coordenadas.get(0) + ", " + coordenadas.get(1);
    }

    @TypeConverter
    public static List<Double> stringToCoordenadas (String coordenadas) {
        if (coordenadas != null) {
            String[] stringCoords = coordenadas.split(",");
            List<Double> doublecoords = new ArrayList<>();
            doublecoords.add(Double.valueOf(stringCoords[0]));
            doublecoords.add(Double.valueOf(stringCoords[1]));
            return doublecoords;
        } else {
            return null;
        }
    }
}
