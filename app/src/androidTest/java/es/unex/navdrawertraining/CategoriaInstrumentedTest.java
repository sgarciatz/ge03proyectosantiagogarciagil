package es.unex.navdrawertraining;

import static org.junit.Assert.assertEquals;

import android.os.Bundle;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;

import es.unex.navdrawertraining.data.modelodatos.Categoria;

@RunWith(AndroidJUnit4.class)
public class CategoriaInstrumentedTest {

    @Test
    public void bundlelizeCategoriaTest() throws NoSuchFieldException, IllegalAccessException {
        final Categoria instance = new Categoria(1L, "nombre", "icono", "color");
        final Field id = instance.getClass().getDeclaredField("id_categoria");
        id.setAccessible(true);

        Bundle bundle = instance.bundlelizeCategoria();

        assertEquals("The bundle does not match the field of the instance", bundle.getLong("id"), id.get(instance));
    }
}